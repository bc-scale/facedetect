import os
import time
import face_recognition
from flask import Flask, request, Response

PATH_IMAGES_DIR = './enrrolados'
enrrolados_faces = []
res = os.listdir(PATH_IMAGES_DIR)
app = Flask(__name__)

@app.route('/')
def index():
    return Response("<html><head><meta name='viewport' content='width=device-width, initial-scale=1.0'><title>Enrrolar</title><script async src='opencv.js' onload='openCvReady();'></script><script src='utils.js'></script><style> .labele { color: white; padding: 8px; font-family: Arial; background-color: #ff9800; } .labelt { color: white; padding: 8px; font-family: Arial; background-color: #04aa6d; } @media only screen and (max-width: 992px) { video.camara { height:640px; width:480px; display: block; margin-left: auto; margin-right: auto; } } @media only screen and (min-width: 993px) { video.camara { height:480px; width:640px; display: block; margin-left: auto; margin-right: auto;} } </style></head><body  bgcolor='#000' oncontextmenu='return false' onkeydown='return false' onload=\"setTimeout('temporizador()',1000)\"> <br><center> <span id='Tiempo' class='labelt'>0</span> <span id='Estado' class='labele'>Iniciando...</span> </center><br><br><center><! canvas id='canvas_output' /><! /canvas> <video id='cam_input' height='480' width='640' class='camara'></video> </center><script>/* variables globales para el funcionamiento */ let tiempo = 0;/* aperturamos webcam con opencv */function openCvReady() {cv['onRuntimeInitialized'] = () => {let video = document.getElementById('cam_input');/*video.style.display='none';*/navigator.mediaDevices.getUserMedia({ video: true, audio: false }).then(function (stream) { video.srcObject = stream; video.play(); }).catch(function (err) { console.log('Error: ' + err); });let src = new cv.Mat(video.height, video.width, cv.CV_8UC4);let gray = new cv.Mat();let cap = new cv.VideoCapture(cam_input); let faces = new cv.RectVector(); let faceClassifier = new cv.CascadeClassifier(); let utils = new Utils('errorMessage');let faceCascade = 'haarcascade_frontalface_default.xml';utils.createFileFromUrl(faceCascade, faceCascade, () => { faceClassifier.load(faceCascade); });const FPS = 40;function processVideo() {let begin = Date.now();cap.read(src);cv.cvtColor(src, gray, cv.COLOR_RGBA2GRAY, 0);let detectado=0;try {faceClassifier.detectMultiScale(gray, faces, 1.1, 3, 0);for (let i = 0; i < faces.size(); ++i) {let face = faces.get(i);let point1 = new cv.Point(face.x, face.y);let point2 = new cv.Point(face.x + face.width, face.y + face.height);cv.rectangle(src, point1, point2, [0, 255, 0, 255]);detectado = 1;}} catch (err) {console.log(err);}if(detectado==1){document.getElementById('Estado').innerHTML = 'Rostro detectado';document.getElementById('Estado').style.backgroundColor='#04aa6d';} else{document.getElementById('Estado').innerHTML = 'Rostro no detectado';document.getElementById('Estado').style.backgroundColor='#f44336';}/*cv.imshow('canvas_output', src);*/let delay = 1000 / FPS - (Date.now() - begin);setTimeout(processVideo, delay);} setTimeout(processVideo, 0);}} /* temporizador que usa la variable global tiempo para contar los segundos */function temporizador() {if(document.getElementById('Estado').textContent=='Rostro detectado'){tiempo = tiempo + 1;document.getElementById('Tiempo').innerHTML = tiempo;} else{tiempo = 0;document.getElementById('Tiempo').innerHTML = tiempo;}/*cuando haya pasado 3 segundos de la deteccion de un rostro ejecutar()*/if(tiempo == 3){ejecutar(); }setTimeout('temporizador()',1000);} /* generamos el archivo de imagen sin el recuadro */ function ejecutar(){ let imageCanvas = document.createElement('canvas'); let imageCtx = imageCanvas.getContext('2d'); let v = document.getElementById('cam_input');imageCanvas.width = v.videoWidth;imageCanvas.height = v.videoHeight;imageCtx.drawImage(v, 0, 0, v.videoWidth, v.videoHeight);imageCanvas.toBlob(postFile, 'image/jpeg');tiempo = 0;} /* enviamos el 'file' y el 'identificador' a la url 'enrrolar' por metodo 'POST' */ function postFile(file) { let formdata = new FormData();let identificador = prompt('Ingrese un identificador:');if(identificador==null){return}tiempo = 0;document.getElementById('Tiempo').innerHTML = tiempo;formdata.append('id', identificador);formdata.append('image', file);let xhr = new XMLHttpRequest();xhr.open('POST', 'enrrolar', true);xhr.onload = function () {if (this.status === 200){alert(this.response);}else{alert(xhr);}};xhr.send(formdata);}</script></body></html>", mimetype="text/html")

@app.route('/identificar.html')
def identificarhtml():
    return Response("<html><head><meta name='viewport' content='width=device-width, initial-scale=1.0'> <title>Identificar</title><script async src='opencv.js' onload='openCvReady();'></script><script src='utils.js'></script><style> .labele { color: white; padding: 8px; font-family: Arial; background-color: #ff9800; } .labelt { color: white; padding: 8px; font-family: Arial; background-color: #04aa6d; } .labeli { color: white; padding: 8px; font-family: Arial; background-color: #f44336; } @media only screen and (max-width: 992px) { video.camara { height:640px; width:480px; display: block; margin-left: auto; margin-right: auto; } } @media only screen and (min-width: 993px) { video.camara { height:480px; width:640px; display: block; margin-left: auto; margin-right: auto;} } </style></head><body  bgcolor='#000' oncontextmenu='return false' onkeydown='return false' onload=\"setTimeout('temporizador()',1000)\"> <br><center> <span id='Tiempo' class='labelt'>0</span> <span id='Estado' class='labele'>Iniciando...</span> <span id='Identificador' class='labeli'>Desconocido</span> </center><br><br><center><! canvas id='canvas_output' /><! /canvas> <video id='cam_input' height='480' width='640' class='camara'></video> </center><script>/* variables globales para el funcionamiento */let identificado = 0; let tiempo = 0;/* aperturamos webcam con opencv */function openCvReady() {cv['onRuntimeInitialized'] = () => {let video = document.getElementById('cam_input');/* video.style.display='none'; */navigator.mediaDevices.getUserMedia({ video: true, audio: false }).then(function (stream) { video.srcObject = stream; video.play(); }).catch(function (err) { console.log('Error: ' + err); });let src = new cv.Mat(video.height, video.width, cv.CV_8UC4);let gray = new cv.Mat();let cap = new cv.VideoCapture(cam_input);let faces = new cv.RectVector();let faceClassifier = new cv.CascadeClassifier();let utils = new Utils('errorMessage');let faceCascade = 'haarcascade_frontalface_default.xml';utils.createFileFromUrl(faceCascade, faceCascade, () => { faceClassifier.load(faceCascade); });const FPS = 40;function processVideo() {let begin = Date.now();cap.read(src);cv.cvtColor(src, gray, cv.COLOR_RGBA2GRAY, 0);let detectado=0;try {faceClassifier.detectMultiScale(gray, faces, 1.1, 3, 0);for (let i = 0; i < faces.size(); ++i) {let face = faces.get(i);let point1 = new cv.Point(face.x, face.y);let point2 = new cv.Point(face.x + face.width, face.y + face.height);cv.rectangle(src, point1, point2, [0, 255, 0, 255]);detectado = 1;}} catch (err) {console.log(err);}if(detectado==1){document.getElementById('Estado').innerHTML = 'Rostro detectado';document.getElementById('Estado').style.backgroundColor='#04aa6d';} else{document.getElementById('Estado').innerHTML = 'Rostro no detectado';document.getElementById('Estado').style.backgroundColor='#f44336';}/*cv.imshow('canvas_output', src);*/ let delay = 1000 / FPS - (Date.now() - begin);setTimeout(processVideo, delay);} setTimeout(processVideo, 0);}}/* temporizador que usa la variable global tiempo para contar los segundos */function temporizador() {if(identificado==0){if(document.getElementById('Estado').textContent=='Rostro detectado'){tiempo = tiempo + 1;document.getElementById('Tiempo').innerHTML = tiempo;} else{tiempo = 0; document.getElementById('Tiempo').innerHTML = tiempo;}/*cuando hayan pasado 3 segundos de la deteccion de un rostro ejecutar()*/if(tiempo == 3){ejecutar(); }}setTimeout('temporizador()',1000);}/* generamos el archivo de imagen sin el recuadro */function ejecutar(){let imageCanvas = document.createElement('canvas');let imageCtx = imageCanvas.getContext('2d');let v = document.getElementById('cam_input');imageCanvas.width = v.videoWidth;imageCanvas.height = v.videoHeight;imageCtx.drawImage(v, 0, 0, v.videoWidth, v.videoHeight);imageCanvas.toBlob(postFile, 'image/jpeg');tiempo = 0;}/* enviamos el 'file' a la url 'identificar' por metodo 'POST' */function postFile(file) { let formdata = new FormData();formdata.append('image', file);let xhr = new XMLHttpRequest();xhr.open('POST', 'identificar', true);xhr.onload = function () {if (this.status === 200){if (this.response != 'NO IDENTIFICADO'){ identificacion(this.response); }}};xhr.send(formdata);}/* mostramos el Identificador de la persona por 5 segundos */async function identificacion(res) {tiempo = 0; document.getElementById('Tiempo').innerHTML = tiempo;identificado = 1;document.getElementById('Identificador').innerHTML = res;document.getElementById('Identificador').style.backgroundColor='#00008b';await sleep(5 * 1000); document.getElementById('Identificador').innerHTML = 'Rostro no detectado';document.getElementById('Identificador').style.backgroundColor='#f44336';identificado = 0;}/* funcion que simula sleep */function sleep(ms) {return new Promise((resolve) => setTimeout(resolve, ms));}</script></body></html>", mimetype="text/html")

@app.route('/opencv.js')
def opencvjs():
    return Response(open('opencv.js').read(), mimetype="text/javascript")

@app.route('/utils.js')
def utilsjs():
    return Response(open('utils.js').read(), mimetype="text/javascript")

@app.route('/haarcascade_frontalface_default.xml')
def haarcascade_frontalface_defaultxml():
    return Response(open('haarcascade_frontalface_default.xml').read(), mimetype="application/xml")

@app.route('/enrrolar', methods=['POST'])
def enrrolar():
    identificador = request.form.get("id")
    file = request.files['image']
    try:
        print("Identificador: "+identificador+" ya existe en posicion: "+res.index(identificador))
        return Response("Ya existe el identificador: "+identificador)
    except:
        try: 
            if file.filename != '':
                file.save('%s/%s' % (PATH_IMAGES_DIR, identificador))
                face_image = face_recognition.load_image_file(PATH_IMAGES_DIR+'/'+identificador)
                unk_face_encoding = face_recognition.face_encodings(face_image)[0]
                enrrolados_faces.append(unk_face_encoding) 
                res.append(identificador) 
                return Response("Enrrolado Exitosamente "+identificador)
            else:
                return Response("Error al obtener el imagen")
        except:
            return Response("Error al enrrolar")

@app.route('/identificar', methods=['POST'])
def identificar():
    try:
        file = request.files['image']
        nuevonombre = ('%s' % time.strftime("%Y%m%d%H%M%S"))
        file.save('%s/%s' % ("./temp", nuevonombre))
        face_image = face_recognition.load_image_file("./temp"+'/'+nuevonombre)
        unk_face_encoding = face_recognition.face_encodings(face_image)[0]
        results = face_recognition.compare_faces(enrrolados_faces, unk_face_encoding, tolerance=0.4)
        return Response( res[results.index(True)] )
    except:
        return Response("NO IDENTIFICADO")

if __name__ == '__main__':
    for x in res:
        face_image = face_recognition.load_image_file(PATH_IMAGES_DIR+'/'+x)
        try:
            unk_face_encoding = face_recognition.face_encodings(face_image)[0]
            enrrolados_faces.append(unk_face_encoding) 
        except:
            print("Imagen no valida: "+x)            
            quit()
    app.run(debug=True, host='0.0.0.0')
