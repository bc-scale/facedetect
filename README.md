# Rostro Biometrico
Este proyecto es un servidor web hecho para la deteccion y reconocimiento de rostros; implementando un conjunto de herramientas escritas en python, javascript y C++.


<p align="center">
  <img alt="Light" src="https://gitlab.com/RicardoValladares/facedetect/-/raw/python/desktop.png" width="50%">
  &nbsp; &nbsp; &nbsp; &nbsp;
  <img alt="Dark" src="https://gitlab.com/RicardoValladares/facedetect/-/raw/python/celphone.png" width="15%">
</p>


Librerias usadas:
- OpenCV (https://docs.opencv.org/3.4/d5/d10/tutorial_js_root.html)
- Face-Recognition (https://github.com/ageitgey/face_recognition)
- Dlib (http://dlib.net/)


<br>
<br>

### Instalar dependencias en FreeBSD
Si heres usuario del sistema opertivo FreeBSD puedes instalar las dependencias ejecutando los siguientes comandos: 

```bash
pkg install python39 py39-dlib
pkg install graphics/py-face_recognition

```

### Instalar dependencias en Linux o MacOS

```bash
pip install dlib
pip install face_recognition

```

### En Windows no es oficialmente compatible, pero puede funcionar:
En el siguiente enlace encontraras como instalar las dependiencias en Windows si quieres editar el codigo: https://www.geeksforgeeks.org/how-to-install-dlib-library-for-python-in-windows-10/

Pero si deseas solo ejecutarlo sin instalar dependencias, puedes ejecutar los siguientes comandos en powershell:

```powershell
Invoke-WebRequest -Uri https://gitlab.com/RicardoValladares/facedetect/-/raw/python/main.zip -OutFile .\main.zip
Expand-Archive -LiteralPath .\main.zip -DestinationPath .\main
cd .\main\
.\main.exe
```
